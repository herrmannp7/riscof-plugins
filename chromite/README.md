## The Chromite Core plugin for RISCOF

This plugin allows you to use the
[Chromite](https://chromite.readthedocs.io/en/latest/) core as a DUT for in
RISCOF to running the RISC-V architectural tests.

### Building a Verilated executable for the core

You will need to first build a verilated executable of the Chromite Core which
can be used for simulations. This will require the tools listed
[here](https://chromite.readthedocs.io/en/latest/getting_started.html#install-dependencies).
The steps are:

```
git clone https://gitlab.com/incoresemi/core-generators/chromite.git
cd chromite
python -m configure.main
make generate_verilog link_verilator generate_boot_files
```
Please refer to
[this](https://chromite.readthedocs.io/en/latest/getting_started.html) document
if you face any issues in the above steps. 

The above steps should create a `chromite_core` executable and a `boot.mem` hex
file in the `chromite/bin/` directory. We will be using this directory path to
setup our python plugin in the config file of RISCOF.

### Entry in Config.ini

Once you have the verilated executable of the chromite, you can now create a new
an entry for this in the `config.ini` file of RISCOF as per the following.
Please remember to change the paths accordingly:

```
[chromite]
pluginpath=/scratch/git-repo/incoresemi/riscof-plugins/chromite/
ispec=/scratch/git-repo/incoresemi/riscof-plugins/chromite/chromite_isa.yaml
pspec=/scratch/git-repo/incoresemi/riscof-plugins/chromite/chromite_platform.yaml
chromitebin=/scratch/git-repo/incoresemi/core-generators/chromite/bin/

```

A sample `config.ini` file is also present in the same directory.

### How the plugin works

This plugin expects the user to build the simulation executable before hand. The
simulation executable should include a test-bench as with all necessary
components to load the tests, run it and extract the signature. 

In the above guide we have used the a very basic
[test-soc](https://gitlab.com/incoresemi/core-generators/chromite/-/tree/master/test_soc)
and test-bench around it to enable running the architectural tests. We have also
compiled the test-bench using verilator. One can choose to use any other
simulator (vcs, verdi, irun, etc) to build a final simulation executable.

The python plugin itself doesn't do much different than the sail/spike plugins.
The major difference is in the runTests function. Here, we create a Makefile
with each target corresponding to a test. Each target will create a soft-link of
the executable and the boot-files to the test's working directory, generate a
`code.mem` hex file from the compiled elf of the test using spike's `elf2hex`
command and finall execute the test.

One can adopt this plugin for other cores or DUTs as well, since this plugin
does not assume any particular test-bench infra-structure other than loading the
test via a hex file.

